# La Disparition #

Projet JAVA 

### Sujet ###

Il s’agit d’un petit jeu graphique vu du dessus pour deux joueu(r/euse)s avec
contrôle au clavier. Chacun des joueu(r/euse)s peut :

 * se déplacer dans les quatre directions
 * tirer avec un pistolet à impulsion électrique
 * provoquer l’extinction des lumières (avec la barre espace)

####  Contexte de l’histoire  ####

Une personne décide d’effacer son profil d’un réseau social, mais la procé-
dure de désactivation du compte se révèle incomplète, et la société est hors
de juridiction de la CNIL. Elle décide de prendre les choses en main et
d’infiltrer le réseau interne de l’entreprise pour effacer elle-même ses données
personnelles nominatives, armée d’un pistolet à impulsions électriques (PIE).
Elle doit pour cela atteindre l’ordinateur connecté au réseau interne et s’y
maintenir consciente pendant 5 secondes consécutives. Ce personnage sera
dénommé ci-après I pour infiltrant.
Une personne est de garde pour protégé le système d’information de la
corporation en charge du réseau social. Elle doit capturer tout intrus (en
l’occurrence le personnage I). Pour cela, elle doit se maintenir au contact de son adversaire pendant 5 secondes consécutives. Elle dispose elle aussi d’un
PIE. Ce personnage sera dénommé ci-après G pour garde.
En début de partie, l’éclairage global est allumé et les personnages sont
donc visibles. Une particularité de ce jeu est qu’il est possible d’éteindre cet
éclairage global et de rendre alors les personnages invisibles.

#### Eclairage Global ####

La particularité de l’action sur la lumière est qu’elle est commune aux deux
joueu(r/euse)s : les deux joueu(r/euse)s ont accès à la même barre espace
pour éteindre les lumières sur tout le niveau de jeu. Si la barre d’espace est
appuyée par l’un et/*ou* l’autre, alors l’éclairage général est éteint.

#### Bornes d'éclairage de secours ####

Il y a aussi des bornes d’éclairage de secours qui éclairent localement (cases
adjacentes) même lorsque l’éclairage général est éteint. Il est possible de
détruire ces bornes en leur tirant dessus.

#### Pistolet à impulsions électriques ####

Chaque joueu(r/euse) dispose d’un pistolet à impulsions électriques disposant
d’un certain nombre de charges (8) avant que la batterie ne soit
épuisée. Ce pistolet a une portée de 4 cases dans la direction de déplacement
du personnage, le pistolet touche seulement la première cible dans la
limite de sa portée. Les cibles possibles sont :

 * l’autre personnage, qui est alors immobilisé pour 4 secondes
 * une borne d’éclairage de secours, qui est alors définitivement détruite
(et devient un passage normal pour le reste de la partie).

Lorsqu’un PIE est utilisé, il émet des étincelles qui révèlent la position
du joueur si l’éclairage était éteint.

#### Conditions de fin de jeu ####

Le jeu se termine lorsque l’une des conditions suivante survient :

 * le personnage I atteint l’ordinateur et s’y maintient 5 secondes
 * le personnage G reste 5 secondes consécutives au contact du personnage
G.

#### Cahier des charges de base ####

Le jeu doit implémenter les règles ci-dessus pour :
 * un niveau de jeu prédéfini donné en annexe
 * d’autres niveaux de jeu.

### Set up ###


```
#!sh
$ cd /directory/to/the/project
$ rm /src
$ git clone git@bitbucket.org:projectinfo/disparition.git .

```
