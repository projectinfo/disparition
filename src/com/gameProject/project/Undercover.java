package com.gameProject.project;

import com.gameProject.listeners.BindTimer;
import com.gameProject.listeners.KeyReleased;

import java.awt.*;
import java.util.Timer;

/**
 * Undercover class
 * @author Jean-Baptiste WATENBERG <a href="mailto:jeanbaptiste.watenberg@gmail.com">jeanbaptiste.watenberg@gmail.com</a>
 * @author Jaswinder GURU <a href="mailto:jaswinderguru@gmail.com">jaswinderguru@gmail.com</a>
 */
public class Undercover extends Player {
    private Color color = Color.blue;
    private Timer timer;

    /**
     * Undercover's constructor
     *
     * @param level index
     * @param x     X position
     * @param y     Y position
     */
    public Undercover(int level, double x, double y) {
        super(level, x, y);
        this.setName("infiltrated");
        new KeyReleased(this);
    }

    /**
     * Use PIE gun
     */
    @Override
    public void usePie() {
        if (this.cartridges > 0) {
            PieCartridge pieCartridge = new PieCartridge(this.x, this.y, this.rotate, level);
            pieCartridge.shoot();
            this.cartridges--;
        }
    }

    /**
     * This player has just moved
     */
    @Override
    public void justMoved() {
        //test 5 positions
        boolean testUp = LevelMap.isServer(this.x, this.y + LevelMap.getCaseHeight(), level);
        boolean testDown = LevelMap.isServer(this.x, this.y - LevelMap.getCaseHeight(), level);
        boolean testRight = LevelMap.isServer(this.x + LevelMap.getCaseWidth(), this.y, level);
        boolean testLeft = LevelMap.isServer(this.x - LevelMap.getCaseWidth(), this.y, level);
        boolean testCurrent = LevelMap.isServer(this.x, this.y, level);

        if (testCurrent || testDown || testLeft || testRight || testUp) {
            //setup the timer
            timer = new Timer();
            timer.schedule(new BindTimer(this), 5000);
        } else {
            if (timer != null) {
                timer.cancel();
            }
        }
    }
}
