package com.gameProject.project;

import com.stdlib.StdDraw;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * LevelMap is the base class that manage all levels action and attributes.
 *
 * @author Jean-Baptiste WATENBERG <a href="mailto:jeanbaptiste.watenberg@gmail.com">jeanbaptiste.watenberg@gmail.com</a>
 * @author Jaswinder GURU <a href="mailto:jaswinderguru@gmail.com">jaswinderguru@gmail.com</a>
 */
public final class LevelMap extends Thread {
    /**
     * Canvas width
     */
    public static final double WIDTH = 750;
    /**
     * Canvas height
     */
    public static final double HEIGHT = 350;
    /**
     * integer corresponding to an empty box
     */
    private static final int EMPTY_CASE = 0;
    /**
     * integer corresponding to a wall
     */
    private static final int WALL_CASE = 1;
    /**
     * integer corresponding to a light
     */
    private static final int ENLIGHT = 2;
    /**
     * integer corresponding to the server
     */
    private static final int SERVER = 3;
    /**
     * levels stand for definition of each level map in the game
     */
    public static int[][][] levels = {
            {
                    {
                            EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, ENLIGHT, EMPTY_CASE, WALL_CASE, EMPTY_CASE, EMPTY_CASE, EMPTY_CASE, EMPTY_CASE, EMPTY_CASE, EMPTY_CASE, EMPTY_CASE
                    },
                    {
                            EMPTY_CASE, EMPTY_CASE, EMPTY_CASE, EMPTY_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE
                    },
                    {
                            EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, EMPTY_CASE, EMPTY_CASE
                    },
                    {
                            EMPTY_CASE, WALL_CASE, ENLIGHT, WALL_CASE, EMPTY_CASE, EMPTY_CASE, ENLIGHT, EMPTY_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE
                    },
                    {
                            EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, ENLIGHT, EMPTY_CASE, EMPTY_CASE, SERVER, EMPTY_CASE, WALL_CASE, EMPTY_CASE
                    },
                    {
                            EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, EMPTY_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE
                    },
                    {
                            EMPTY_CASE, EMPTY_CASE, ENLIGHT, EMPTY_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE, WALL_CASE, EMPTY_CASE
                    },
            }
    };
    /**
     * x index in default position array for the guard and the undercover player
     */
    private static final int X = 0;
    /**
     * y index in default position array for the guard and the undercover player
     */
    private static final int Y = 1;
    /**
     * Block width
     * 1/15 = max scale x / number of blocks on x axis - an offset that center the game
     */
    private static final double CASE_WIDTH = 0.06662 - 0.01;
    /**
     * Block height
     * 1/7 = max scale Y / number of blocks on y axis - an offset that center the game
     */
    private static final double CASE_HEIGHT = 0.14285714 - 0.01;
    /**
     * false = lights switch on, true = lights switch off
     */
    public static boolean isBlack = false;
    /**
     * defaults positions of the undercover player for each
     */
    private static int[][] defaultPositionInfiltrated = {
            {0, 0}
    };
    /**
     * defaults positions of the guard player for each levels
     */
    private static int[][] defaultPositionGuard = {
            {14, 0}
    };
    /**
     * infiltrated player
     */
    private static Player infiltrated;
    /**
     * guard player
     */
    private static Player guard;
    /**
     * Index of the current level
     */
    private int index = 0;
    
    /**
     * Class constructor
     *
     * @param index int the level index
     */
    public LevelMap(int index) {
        StdDraw.setCanvasSize((int) WIDTH, (int) HEIGHT);
        this.index = index;
    }

    /**
     * This method allow you to generate the level map based on the levels array.
     * The index parameter correspond to the index of the level that you want generate.
     * @param index int the level index
     */
    public static void generateLevel(int index) {
        int[][] currentLevel = levels[index];
        for (int i = 0; i < currentLevel.length; ++i) {
            for (int j = 0; j < currentLevel[i].length; ++j) {
                switch (currentLevel[i][j]) {
                    case EMPTY_CASE:
                        if (isBlack) {
                            StdDraw.setPenColor(Color.BLACK);//if the current block is empty but the light is switched off we set the color to black
                        } else {
                            StdDraw.setPenColor(Color.white);//if the current block is empty we set the color to black
                        }
                        break;
                    case WALL_CASE:
                        StdDraw.setPenColor(Color.red);//if the current block is a wall we set the color to red
                        break;
                    case ENLIGHT:
                        StdDraw.setPenColor(Color.yellow);//if the current block is a light we set the color to yellow
                        break;
                    case SERVER:
                        StdDraw.setPenColor(Color.green);//if the current block is a server we set the color to green
                        break;
                }
                drawRect(j, i);// we draw the rectangle at position {x = j, y = i}
            }
        }
    }

    /**
     * Draw a block at position {x,y}
     * @param x int the x position
     * @param y int the y position
     */
    public static void drawRect(int x, int y) {
        StdDraw.filledRectangle(convertX(x), convertY(y), getCaseWidth() / 2, getCaseHeight() / 2);
    }

    /**
     * Convert the integer x to double according to the grid
     * @param x int the integer to convert
     * @return double
     */
    public static double convertX(int x) {
        return x * getCaseWidth() + getCaseWidth();//x * CASE_XIDTH = x position of the block  + CASE_WIDTH = correct a bug at rendering the level by adding an offset
    }

    /**
     * Convert the integer y to double according to the grid
     * @param y int the integer to convert
     * @return double
     */
    public static double convertY(int y) {
        return 1 - y * getCaseHeight() - getCaseHeight();//1 - y * CASE_HEIGHT = y position of the block (the origin of y axis is the bottom left corner) - CASEHEIGHT = correct a bug at rendering the level by adding an offset
    }

    /**
     * Do the opposite operation than {@link #convertX(int)}
     * @param x double
     * @return int
     */
    public static int convertX(double x) {
        return (int) Math.round((x - getCaseWidth()) / getCaseWidth());
    }

    /**
     * Do the opposite operation than {@link #convertY(int)}
     * @param y double
     * @return int
     */
    public static int convertY(double y) {
        return (int) Math.round((-y - getCaseHeight() + 1) / getCaseHeight());
    }

    /**
     * Check if the move of a player is allowed
     * @param x double  x position
     * @param y double  y position
     * @param index int level index
     * @return boolean
     */
    public static boolean checkMove(double x, double y, int index) {
        if (x - getCaseWidth() < 0 || y + getCaseHeight() < 0 || x > getCaseWidth() * 15 || y > getCaseHeight() * 9) { // check if the x or the y is out of the layer's bounds
            return false; // False if out of the layout
        }
        int[][] currentLevel = levels[index];

        int trueX = convertX(x);
        if (trueX > 14) {
            return false;
        } else if (trueX < 0) {
            return false;
        }
        int trueY = convertY(y);
        if (trueY > 6) {
            return false;
        } else if (trueY < 0) {
            return false;
        }

        return currentLevel[trueY][trueX] != WALL_CASE;// true if the case isn't a wall
    }

    /**
     * Check if the block at x and y position is a light
     * @param x double  the x position
     * @param y double  the y position
     * @param index int the level index
     * @return boolean  true if the block is a light
     */
    public static boolean isEnlight(double x, double y, int index) {
        int[][] currentLevel = levels[index];

        int trueX = convertX(x);
        if (trueX > 14) {
            return false;
        } else if (trueX < 0) {
            return false;
        }
        int trueY = convertY(y);
        if (trueY > 6) {
            return false;
        } else if (trueY < 0) {
            return false;
        }


        return currentLevel[trueY][trueX] == ENLIGHT;// true if the case is a enlight
    }

    /**
     * This method break a light
     * @param x double  the x position
     * @param y double  the y position
     * @param index int the level index
     * @return boolean  true if the block was a light
     */
    public static boolean breakLight(double x, double y, int index) {
        if (isEnlight(x, y, index)) {
            int trueX = convertX(x);
            int trueY = convertY(y);

            levels[index][trueY][trueX] = EMPTY_CASE;
            return true;
        }
        return false;
    }

    /**
     * Check if the block at x and y position is a server
     * @param x double  the x position
     * @param y double  the y position
     * @param index int the level index
     * @return boolean  true if the block is a server
     */
    public static boolean isServer(double x, double y, int index) {
        int[][] currentLevel = levels[index];

        int trueX = convertX(x);
        if (trueX > 14) {
            return false;
        } else if (trueX < 0) {
            return false;
        }
        int trueY = convertY(y);
        if (trueY > 6) {
            return false;
        } else if (trueY < 0) {
            return false;
        }


        return currentLevel[trueY][trueX] == SERVER;// true if the case is a server
    }

    /**
     * This get the player position
     * @param playerType    infiltrated||guard
     * @return double[] positionX,positionY
     */
    public static double[] getPlayerPosition(String playerType) {
        double[] pos = new double[2];
        if (playerType.equals("guard")) {
            pos[0] = LevelMap.convertX(LevelMap.convertX(guard.getX()));
            pos[1] = LevelMap.convertY(LevelMap.convertY(guard.getY()));
        } else if (playerType.equals("infiltrated")) {
            pos[0] = LevelMap.convertX(LevelMap.convertX(infiltrated.getX()));
            pos[1] = LevelMap.convertY(LevelMap.convertY(infiltrated.getY()));
        } else {
            try {
                throw new Exception("Personnage inconnu");
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return pos;
    }

    /**
     * This method return infiltrated or guard or none according to the player who are at the position (x,y).
     * none is returned when there is no player at the position
     *
     * @param x double  the x position
     * @param y double  the y position
     * @return string none|infiltrated|guard
     */
    public static String getPLayerAt(double x, double y) {
        x = LevelMap.convertX(LevelMap.convertX(x));
        y = LevelMap.convertY(LevelMap.convertY(y));
        double[] infiltatredPos = getPlayerPosition("infiltrated");
        double[] infiltratedGuard = getPlayerPosition("guard");
        if (infiltatredPos[X] == x && infiltatredPos[Y] == y) {
            System.out.println("Found an infiltrated");
            return "infiltrated";
        } else if (infiltratedGuard[X] == x && infiltratedGuard[Y] == y) {
            System.out.println("Found a guard");
            return "guard";
        }
        System.out.println("Found none");
        return "none";
    }

    /**
     * get the width of a block
     * @return double block's width
     */
    public static double getCaseWidth() {
        return CASE_WIDTH;
    }

    /**
     * get the height of a block
     * @return double block's height
     */
    public static double getCaseHeight() {
        return CASE_HEIGHT;
    }

    /**
     * get the infiltrated player
     * @return infiltrated player
     */
    public static Player getInfiltrated() {
        return infiltrated;
    }

    /**
     * set the infiltrated player
     * unused
     * @param infiltrated
     */
    public static void setInfiltrated(Player infiltrated) {
        LevelMap.infiltrated = infiltrated;
    }

    /**
     * get the guardian
     * @return guardian
     */
    public static Player getGuard() {
        return guard;
    }

    /**
     * set the guardian
     * unused
     * @param guard
     */
    public static void setGuard(Player guard) {
        LevelMap.guard = guard;
    }

    /**
     * This method initialize the level.
     * That means that it call generateLevel and instantiates the guard and the undercover classes.
     * At least it calls the start method of theses both classes.
     */
    public void initializeLevel() {
        generateLevel(index);
        int[] positionInflitrated = defaultPositionInfiltrated[index];
        int[] positionGuard = defaultPositionGuard[index];
        infiltrated = new Undercover(index, convertX(positionInflitrated[X]), convertY(positionInflitrated[Y]));
        guard = new Guards(index, convertX(positionGuard[X]), convertY(positionGuard[Y]));

        infiltrated.start();
        guard.start();
    }

    /**
     * This method run an infinite loop to bind the space key pressed event and trigger the switch of the lights
     * This method run in a new thread to allow people move that need also an infinite loop.
     */
    @Override
    public void run() {
        while (true) {
            if (StdDraw.isKeyPressed(KeyEvent.VK_SPACE)) {
                if (!isBlack) {
                    isBlack = true;
                    generateLevel(index);
                    StdDraw.show(0);
                }
            } else {
                if (isBlack) {
                    isBlack = false;
                    generateLevel(index);
                    guard.showPlayer();
                    infiltrated.showPlayer();
                    StdDraw.show(0);
                }

            }
        }
    }
}
