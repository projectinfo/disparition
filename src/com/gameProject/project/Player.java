package com.gameProject.project;

import com.stdlib.StdDraw;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Player's class
 * @author Jean-Baptiste WATENBERG <a href="mailto:jeanbaptiste.watenberg@gmail.com">jeanbaptiste.watenberg@gmail.com</a>
 * @author Jaswinder GURU <a href="mailto:jaswinderguru@gmail.com">jaswinderguru@gmail.com</a>
 */
public abstract class Player extends Thread {
    /**
     * key code of the key that move up the player
     */
    protected int upKey = KeyEvent.VK_UP;
    /**
     * key code of the key that move down the player
     */
    protected int downKey = KeyEvent.VK_DOWN;
    /**
     * key code of the key that move left the player
     */
    protected int leftKey = KeyEvent.VK_LEFT;
    /**
     * key code of the key that move right the player
     */
    protected int rightKey = KeyEvent.VK_RIGHT;
    /**
     * key code of the key that shoot sth with the PIE
     */
    protected int pieKey = KeyEvent.VK_ENTER;
    /**
     * X position
     */
    protected double x;
    /**
     * Y position
     */
    protected double y;
    /**
     * Previous X position
     */
    protected double prevX = 0;
    /**
     * Previous Y position
     */
    protected double prevY = 0;
    /**
     * Picture width and height
     */
    protected double width, height;
    /**
     * picture url
     */
    protected String urlPicture;
    /**
     * Color
     */
    protected Color color = Color.blue;
    /**
     * Speed
     */
    protected double speed = 0.002;
    /**
     * Number of PIE's cartridge
     */
    protected int cartridges = 8;
    /**
     * Current level index
     */
    protected int level = 0;
    
    /**
     * Current player rotation
     */
    protected int rotate = 0;//initialement vers a gauche

    /**
     * Is the player mobil ?
     */
    protected boolean mobil = true;

    /**
     * Player's name
     */
    protected String playerName = "infiltrated";

    /**
     * Constructor without parameters
     */
    public Player() {

    }

    /**
     * @param level int the level index
     * @param x     double  the default x position
     * @param y     double  the default y position
     */
    public Player(int level, double x, double y) {
        this.level = level;
        this.x = x;
        this.y = y;
        showPlayer();
    }

    /**
     * This method show the player on the map
     */
    public void showPlayer() {
        if (!LevelMap.isBlack) {//we show the players only if the light is on
            StdDraw.setPenColor(color);
            LevelMap.drawRect(LevelMap.convertX(x), LevelMap.convertY(y));
        }
    }

    /**
     * Infinite loop that bind move keys event and trigger the appropriate move.
     * It also bind PIE key event.
     */
    @Override
    public void run() {
        while (true) {
            if (mobil) {
                if (StdDraw.isKeyPressed(this.getUpKey())) {
                    move(this.getUpKey());
                } else if (StdDraw.isKeyPressed(this.getDownKey())) {
                    move(this.getDownKey());
                } else if (StdDraw.isKeyPressed(this.getLeftKey())) {
                    move(this.getLeftKey());
                } else if (StdDraw.isKeyPressed(this.getRightKey())) {
                    move(this.getRightKey());
                }
                /*if (StdDraw.nextKeyTyped() == this.getPieKey()) {
                    usePie();
                }*/
            }
        }
    }

    /**
     * Move the player according to the struck key
     * @param moveType int Code of the struck key
     */
    private void move(int moveType) {
        prevX = x;
        prevY = y;
        if (moveType == this.getUpKey()) {
            if (LevelMap.checkMove(x, y + speed, level) && LevelMap.getPLayerAt(x, y + LevelMap.getCaseHeight()).equals("none")) {//if the movement is allowed
                y = y + speed;//we move the player in the appropriate direction
                rotate = -90;
            }

        } else if (moveType == this.getDownKey()) {
            if (LevelMap.checkMove(x, y - speed, level) && LevelMap.getPLayerAt(x, y - LevelMap.getCaseHeight()).equals("none")) {
                y = y - speed;
                rotate = 90;
            }

        } else if (moveType == this.getLeftKey()) {
            if (LevelMap.checkMove(x - speed, y, level) && LevelMap.getPLayerAt(x - LevelMap.getCaseWidth(), y).equals("none")) {
                x = x - speed;
                rotate = 180;
            }

        } else if (moveType == this.getRightKey() && LevelMap.getPLayerAt(x + LevelMap.getCaseWidth(), y).equals("none")) {
            if (LevelMap.checkMove(x + speed, y, level)) {
                x = x + speed;
                rotate = 0;
            }
        }
        justMoved();
        draw();//we draw the player
        StdDraw.show(0);
    }

    /**
     * Draw the player
     */
    private void draw() {
        if (LevelMap.isBlack) {
            StdDraw.setPenColor(Color.black); //if the light is switched off than the background is black
        } else {
            StdDraw.setPenColor(Color.WHITE); // if the light is switched on then the background is white
        }

        if (LevelMap.isEnlight(prevX, prevY, level)) {//if the prev block is a light we redraw it
            StdDraw.setPenColor(Color.yellow);
            LevelMap.drawRect(LevelMap.convertX(prevX), LevelMap.convertY(prevY));
        } else if (LevelMap.isServer(prevX, prevY, level)) { // if the prev block is a server we redraw it
            StdDraw.setPenColor(Color.green);
            LevelMap.drawRect(LevelMap.convertX(prevX), LevelMap.convertY(prevY));
        } else {
            LevelMap.drawRect(LevelMap.convertX(prevX), LevelMap.convertY(prevY));
        }

        if (LevelMap.isBlack) {
            if (LevelMap.isEnlight(prevX, prevY, level)
                    || LevelMap.isEnlight(prevX + LevelMap.getCaseWidth(), prevY, level)
                    || LevelMap.isEnlight(prevX - LevelMap.getCaseWidth(), prevY, level)
                    || LevelMap.isEnlight(prevX, prevY + LevelMap.getCaseHeight(), level)
                    || LevelMap.isEnlight(prevX, prevY - LevelMap.getCaseHeight(), level)) {
                StdDraw.setPenColor(this.getColor());// if the light is switched off but the player is under secure-lights or near to this light than his color is his
            } else {
                //These comments were to redraw the opponent player if the current player walk on him but it's not further need to be since i've decided that a player can't walk on the other
                /*if (LevelMap.getPLayerAt(prevX,prevY).equals(getOpponentName())) {//if the prev block is a player
                    if (getPlayerName().equals("guard")) {
                        StdDraw.setPenColor(LevelMap.getInfiltrated().getColor());//we get the opponent's color
                    } else {
                        StdDraw.setPenColor(LevelMap.getGuard().getColor());
                    }
                    LevelMap.drawRect(LevelMap.convertX(prevX), LevelMap.convertY(prevY));//we redraw the player
                }*/
                StdDraw.setPenColor(Color.black);// if the light is switched off than the player is black
            }
        } else {
            StdDraw.setPenColor(this.getColor());// if the light is switched on than the player is coloured by his color
        }
        LevelMap.drawRect(LevelMap.convertX(x), LevelMap.convertY(y));
    }

    /**
     * Immobilize this player for 4 seconds
     */
    public void immobilize() {
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start <= 4000) {
            mobil = false;
        }
        mobil = true;
    }

    /**
     * Return the up key code
     * @return int
     */
    public int getUpKey() {
        return upKey;
    }

    /**
     * Set the up key code
     * @param upKey int
     */
    public void setUpKey(int upKey) {
        this.upKey = upKey;
    }

    /**
     * Return the down key code
     * @return int
     */
    public int getDownKey() {
        return downKey;
    }

    /**
     * Set the down key code
     * @param downKey int
     */
    public void setDownKey(int downKey) {
        this.downKey = downKey;
    }

    /**
     * Return the left key code
     * @return int
     */
    public int getLeftKey() {
        return leftKey;
    }

    /**
     * Set the left key code
     * @param leftKey int
     */
    public void setLeftKey(int leftKey) {
        this.leftKey = leftKey;
    }

    /**
     * Return the right key code
     * @return int
     */
    public int getRightKey() {
        return rightKey;
    }

    /**
     * Set the right key code
     * @param rightKey int
     */
    public void setRightKey(int rightKey) {
        this.rightKey = rightKey;
    }

    /**
     * Return the PIE key code
     *
     * @return int
     */
    public int getPieKey() {
        return pieKey;
    }

    /**
     * Set the pie key code
     *
     * @param pieKey int
     */
    public void setPieKey(int pieKey) {
        this.pieKey = pieKey;
    }

    /**
     * Return the player's color
     * @return Color
     */
    public Color getColor() {
        return color;
    }

    /**
     * Set the player's color
     * @param color Color
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * get the player's x coordinate
     *
     * @return x
     */
    public double getX() {
		return x;
	}

	/*public void setX(double x) {
		this.x = x;
	}*/

    /**
     * get the player's y coordinate
     * @return y
     */
    public double getY() {
		return y;
	}

	/*public void setY(double y) {
		this.y = y;
	}*/

	/**
     * Use PIE gun
     */
    public abstract void usePie();

    /**
     * This player has just moved
     */
    public abstract void justMoved();

    /**
     * get the player's playerName
     *
     * @return string that's represent the player's playerName
     */
    public String getPlayerName() {
        return playerName;
    }

    /**
     * set the player's playerName
     *
     * @param name the player's playerName
     */
    public void setPlayerName(String name) {
        this.playerName = name;
    }

    /**
     * get the opponent name
     * @return string guard|infiltrated
     */
    public String getOpponentName() {
        return getPlayerName().equals("infiltrated") ? "guard" : "infiltrated";
    }
}
