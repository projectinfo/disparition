package com.gameProject.project;

import com.stdlib.StdDraw;

/**
 * Pie's cartridge
 *
 * @author Jean-Baptiste WATENBERG <a href="mailto:jeanbaptiste.watenberg@gmail.com">jeanbaptiste.watenberg@gmail.com</a>
 * @author Jaswinder GURU <a href="mailto:jaswinderguru@gmail.com">jaswinderguru@gmail.com</a>
 */

public class PieCartridge {

    private static int level = 0;
    private final double deltaX = LevelMap.getCaseWidth() / 15;
    private final double deltaY = LevelMap.getCaseHeight() / 15;
    private int rotate = 0;
    private double x = 0;
    private double y = 0;
    private int iteratedBlocks = 0;

    /**
     * PieCartridge's constructor
     *
     * @param x      the x position
     * @param y      the y position
     * @param rotate the current rotation angle
     * @param level  the level index
     */
    public PieCartridge(double x, double y, int rotate, int level) {
        this.rotate = rotate;
        this.x = x + deltaX;
        this.y = y + deltaY;
        PieCartridge.level = level;
        debug("Shoot 1 time");
    }

    /**
     * Debug X, Y, R
     * @param message
     */
    public void debug(String message) {
        System.out.println(message);
        System.out.println("X : " + this.x);
        System.out.println("Y : " + this.y);
        System.out.println("R : " + this.rotate);
    }

    /**
     * shoot a PIE cartridge
     */
    public void shoot() {
        boolean touched = false;
        while ((iteratedBlocks < 8) && !touched) {

            String check = "wall";
            if (rotate == 0) { //if the player is on right profile
                check = checkPlayerAndWall(x + LevelMap.getCaseWidth(), y);
                if (check.equals("none")) {
                    x += LevelMap.getCaseWidth();
                    StdDraw.picture(LevelMap.convertX(LevelMap.convertX(x)), LevelMap.convertY(LevelMap.convertY(y)), "resources/laser.png", LevelMap.getCaseWidth(), LevelMap.getCaseHeight() / 16, rotate);
                    debug("Right");
                }
            } else if (rotate == -90) {
                check = checkPlayerAndWall(x, y + LevelMap.getCaseHeight());
                if (check.equals("none")) {
                    y += LevelMap.getCaseWidth();
                    StdDraw.picture(LevelMap.convertX(LevelMap.convertX(x)), LevelMap.convertY(LevelMap.convertY(y)), "resources/laser.png", LevelMap.getCaseWidth(), LevelMap.getCaseHeight() / 16, rotate);
                    debug("Up");
                }
            } else if (rotate == 180) {
                check = checkPlayerAndWall(x - LevelMap.getCaseWidth(), y);
                if (check.equals("none")) {
                    x -= LevelMap.getCaseWidth();
                    StdDraw.picture(LevelMap.convertX(LevelMap.convertX(x)), LevelMap.convertY(LevelMap.convertY(y)), "resources/laser.png", LevelMap.getCaseWidth(), LevelMap.getCaseHeight() / 16, rotate);
                    debug("Left");
                }
            } else if (rotate == 90) {
                check = checkPlayerAndWall(x, y - LevelMap.getCaseHeight());
                if (check.equals("none")) {
                    y -= LevelMap.getCaseWidth();
                    StdDraw.picture(LevelMap.convertX(LevelMap.convertX(x)), LevelMap.convertY(LevelMap.convertY(y)), "resources/laser.png", LevelMap.getCaseWidth(), LevelMap.getCaseHeight() / 16, rotate);
                    debug("Down");
                }
            }

            if (check.equals("infiltrated")) {//if there is the infiltrated player at the next position
                LevelMap.getInfiltrated().immobilize();//we immobilize the player
                touched = true;//we stop the cartridge
                debug("Infiltrated");
            } else if (check.equals("guard")) {
                LevelMap.getGuard().immobilize();
                touched = true;
                debug("Guard");
            }

            if (LevelMap.breakLight(x, y, level)) {//if there is a light at the next position we delete it
                LevelMap.generateLevel(level);//and we re-generate the level
                LevelMap.getGuard().showPlayer();//and we show the infiltrated player
                LevelMap.getInfiltrated().showPlayer();//and the guardian
                touched = true;//at last we stop the cartridge
            }

            StdDraw.show(0);

            iteratedBlocks++;//we shoot at 4 block max
        }

    }

    /**
     * Check if there is a wall or a player at the position (x,y)
     * @param x the x position
     * @param y the y position
     * @return string wall|none|infiltrated|guard
     */
    private String checkPlayerAndWall(double x, double y) {
        if (LevelMap.checkMove(LevelMap.convertX(LevelMap.convertX(x)), LevelMap.convertY(LevelMap.convertY(y)), level)) {
            return LevelMap.getPLayerAt(LevelMap.convertX(LevelMap.convertX(x)), LevelMap.convertY(LevelMap.convertY(y)));
        }
        return "wall";
    }

}