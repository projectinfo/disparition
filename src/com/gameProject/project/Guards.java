package com.gameProject.project;

import com.gameProject.listeners.BindTimer;
import com.gameProject.listeners.KeyReleased;
import com.stdlib.StdDraw;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Timer;

/**
 * Guards class
 *
 * @author Jean-Baptiste WATENBERG <a href="mailto:jeanbaptiste.watenberg@gmail.com">jeanbaptiste.watenberg@gmail.com</a>
 * @author Jaswinder GURU <a href="mailto:jaswinderguru@gmail.com">jaswinderguru@gmail.com</a>
 */
public class Guards extends Player {
    /**
     * When we pressed this key the player move up
     */
    protected int upKey = KeyEvent.VK_Z;
    /**
     * When we pressed this key th player move down
     */
    protected int downKey = KeyEvent.VK_S;
    /**
     * When we pressed this key the player move left
     */
    protected int leftKey = KeyEvent.VK_Q;
    /**
     * When we pressed this key the player move right
     */
    protected int rightKey = KeyEvent.VK_D;
    /**
     * When we pressed this key the player use his PIE
     */
    protected int pieKey = KeyEvent.VK_F;
    /**
     * This is the player's color
     */
    protected Color color = Color.orange;
    /**
     * This is the timer that trigger end event
     */
    private Timer timer;

    /**
     * Guard's constructor
     *
     * @param level Current level index
     * @param x     The x position
     * @param y     The y position
     */
    public Guards(int level, double x, double y) {
        this.setDownKey(downKey);
        this.setLeftKey(leftKey);
        this.setRightKey(rightKey);
        this.setUpKey(upKey);
        this.setPieKey(pieKey);
        this.setColor(color);
        this.setPlayerName("guard");
        this.level = level;
        this.x = x;
        this.y = y;
        StdDraw.setPenColor(color);
        LevelMap.drawRect(LevelMap.convertX(x), LevelMap.convertY(y));
        new KeyReleased(this);
    }

    /**
     * Use PIE gun
     */
    @Override
    public void usePie() {
        if (this.cartridges > 0) {
            PieCartridge pieCartridge = new PieCartridge(this.x, this.y, this.rotate, level);
            pieCartridge.shoot();
            this.cartridges--;
        }
    }

    /**
     * This player has just moved
     */
    @Override
    public void justMoved() {
        //test in 4 direction
        String testRight = LevelMap.getPLayerAt(this.x + LevelMap.getCaseWidth(), this.y);
        String testLeft = LevelMap.getPLayerAt(this.x - LevelMap.getCaseWidth(), this.y);
        String testUp = LevelMap.getPLayerAt(this.x, this.y + LevelMap.getCaseHeight());
        String testDown = LevelMap.getPLayerAt(this.x, this.y - LevelMap.getCaseHeight());

        if (!testRight.equals("none") || !testDown.equals("none") || !testLeft.equals("none") || !testUp.equals("none")) {
            //setup the timertimer = new Timer();
            timer = new Timer();
            timer.schedule(new BindTimer(this), 5000);
        } else {
            if (timer != null) {
                timer.cancel();
            }
        }
    }
}
