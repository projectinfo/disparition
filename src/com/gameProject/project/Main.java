package com.gameProject.project;

/**
 * Main class
 * @author Jean-Baptiste WATENBERG <a href="mailto:jeanbaptiste.watenberg@gmail.com">jeanbaptiste.watenberg@gmail.com</a>
 * @author Jaswinder GURU <a href="mailto:jaswinderguru@gmail.com">jaswinderguru@gmail.com</a>
 */
public class Main {
    public static void main(String[] args) {
        LevelMap map = new LevelMap(0);
        map.start();
        map.initializeLevel();
        //StdDraw.show(1000/24);
        //StdDraw.picture(CASE_WIDTH/2,CASE_HEIGHT/2,"resources/Wall.png");
    }
}
