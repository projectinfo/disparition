package com.gameProject.project;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Unused uinterface
 *
 * @deprecated
 */
public interface PlayerBase {
    final int UP_KEY = KeyEvent.VK_UP;
    final int DOWN_KEY = KeyEvent.VK_DOWN;
    final int LEFT_KEY = KeyEvent.VK_LEFT;
    final int RIGHT_KEY = KeyEvent.VK_RIGHT;
    double x = 0;
    double y = 0;
    Color color = Color.blue;
    double speed = 0.001;
    int cartridges = 8;
    int level = 0;
}
