package com.gameProject.listeners;

import com.gameProject.project.Player;
import com.stdlib.StdDraw;

import java.util.TimerTask;

/**
 * TimerTask is the class to listen 5 second until win
 *
 * @author Jean-Baptiste WATENBERG <a href="mailto:jeanbaptiste.watenberg@gmail.com">jeanbaptiste.watenberg@gmail.com</a>
 * @author Jaswinder GURU <a href="mailto:jaswinderguru@gmail.com">jaswinderguru@gmail.com</a>
 */
public class BindTimer extends TimerTask {

    /**
     * The player that's currently at the point to win
     */
    private Player player;

    /**
     * BindTimer's constructor
     *
     * @param player The player that's currently at the point to win
     */
    public BindTimer(Player player) {
        this.player = player;
    }

    /**
     * The action to be performed by this timer task.
     */
    @Override
    public void run() {
        StdDraw.text(0.5, 0.5, "Players " + this.player.getPlayerName() + " win !");
    }
}
