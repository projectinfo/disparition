package com.gameProject.listeners;

import com.gameProject.project.Player;
import com.stdlib.StdDraw;

/**
 * KeyReleased is the class that trigger key pressed event
 *
 * This class isn't really essentially but it works as it.
 * An other way to do that it to define the isKeyReleased into the player's class
 * and call it from the runn method of this same class.
 *
 * @author Jean-Baptiste WATENBERG <a href="mailto:jeanbaptiste.watenberg@gmail.com">jeanbaptiste.watenberg@gmail.com</a>
 * @author Jaswinder GURU <a href="mailto:jaswinderguru@gmail.com">jaswinderguru@gmail.com</a>
 */
public class KeyReleased extends Thread {

    /**
     * the player to bind pie usage
     */
    private Player player;

    /**
     * KeyReleased's constructor
     *
     * @param player the player to bind pie usage
     */
    public KeyReleased(Player player) {
        this.player = player;
        this.start();
    }

    /**
     * infinite loop to bind pie usage
     */
    public void run() {
        while (true) {
            if (isKeyReleased(player.getPieKey())) {
                player.usePie();
            }
        }
    }

    /**
     * Check if a key is released
     *
     * @param key   key code (example : KeyEvent.VK_ENTER)
     * @return boolean true if the key is released
     */
    public boolean isKeyReleased(int key) {
        if (!StdDraw.isKeyPressed(key)) {
            return false;
        } else {
            while (StdDraw.isKeyPressed(key)) {
            }
            return true;
        }
    }
}
